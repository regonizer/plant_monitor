FROM python:3.9
# use python 3.9 to support gattlib
RUN mkdir -p /app
WORKDIR /app

RUN apt update -q --fix-missing && apt install -yq \
    libboost-python-dev \
    libboost-thread-dev \
    libglib2.0-dev \
    openssh-server \
    && rm -rf /var/lib/apt/lists/*

RUN python -m pip install \
    gattlib \
    influxdb-client \
    matrix-nio

ADD ./assets/sshd_config /etc/ssh/sshd_config
ADD ./app /app
CMD ["python", "/main.py"]
