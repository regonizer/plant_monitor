# Plant Monitor

## Hardware

- [ ] Raspberry Pi 4 https://www.raspberrypi.com/products/raspberry-pi-4-model-b/
- [ ] MiFlora Plant Sensor https://www.aliexpress.com/item/1005001483886591.html?spm=a2g0s.9042311.0.0.c8364c4dRluaDI

The provided AliExpress link ships NOT a Xiaomi sensor, it's a copy (you can figure this out by reviewing the used MAC
Address). Any Flower care sensor using the MiFlora Communication protocol is fine. Using unofficial hardware helps to
reduce costs.

## Software

- [ ] Ubuntu Server 20.04.3 LTS https://ubuntu.com/download/raspberry-pi
  (flash it onto a microSD card) (do not use a Desktop Version, the pre-installed bluetooth daemon blocks/reserves the
  module)
- [ ] (Optional) Setup WiFi https://ubuntu.com/core/docs/networkmanager/configure-wifi-connections
- [ ] Update ``` sudo apt update ``` (after startup, an update process is probably running, that's ok, just wait...)
- [ ] Install dependencies (to clone the repo) ``` sudo apt install git curl ```
- [ ] Setup repository in /app or elsewhere (setup.sh installs docker as explained on docker.com and initializes .env file):

```  
git clone https://gitlab.com/regonizer/plant_monitor /app
cd /app
sudo /app/setup.sh
```

- [ ] (Optional) add your user to the docker group (prevents to type sudo every
  time) https://infoheap.com/docker-linux-add-user-to-docker-group/
- [ ] Adjust settings in .env file ```nano .env```
- [ ] Build and run everything ```sudo /app/up.sh```
- [ ] (Optional) Check logs ```docker logs -f plant```