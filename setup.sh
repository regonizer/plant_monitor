#!/bin/bash
cd "$(dirname "$0")"
# https://docs.docker.com/engine/install/ubuntu/
# https://docs.docker.com/compose/install/
# use compose 2.x.x to support arm arch64
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo curl -L "https://github.com/docker/compose/releases/download/2.1.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# setup .env file
printf "MATRIX_USER=myusername\nMATRIX_PASSWORD=mypassword\nINFLUXDB=http://localhost:80\nINFLUXDB_TOKEN=mytoken" > .env
