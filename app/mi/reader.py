#!/usr/bin/env python
import time
from gattlib import GATTRequester, GATTResponse

_HANDLE_DEVICE_NAME = 0x03
_HANDLE_DEVICE_TIME = 0x41
_HANDLE_DATA_READ = 0x35
_HANDLE_MODE_CHANGE = 0x33
_HANDLE_FIRMWARE_AND_BATTERY = 0x38
_HANDLE_HISTORY_CONTROL = 0x3e
_HANDLE_HISTORY_READ = 0x3c

_CMD_BLINK_LED = bytes([0xfd, 0xff])
_CMD_REAL_TIME_READ_INIT = bytes([0xa0, 0x1f])
_CMD_HISTORY_READ_INIT = bytes([0xa0, 0x00, 0x00])
_CMD_HISTORY_READ_SUCCESS = bytes([0xa2, 0x00, 0x00])
_CMD_HISTORY_READ_FAILED = bytes([0xa3, 0x00, 0x00])

_BYTE_ORDER = 'little'

class Reader:
  '''
  Represents a real time entry of sensor values by parsing the byte array returned by the device.

  The sensor returns 16 bytes in total.
  It's unclear what the meaning of these bytes is beyond what is decoded in this method.

  Semantics of the data (in little endian encoding):
  bytes   0-1: temperature in 0.1 °C
  byte      2: unknown
  bytes   3-6: brightness in lux
  byte      7: moisture in %
  byted   8-9: conductivity in µS/cm
  bytes 10-15: unknown
  '''

  def __init__(self, device):
    self.req = GATTRequester(device.mac)
    self.req.read_by_handle(0x15)
    self.req.write_by_handle(_HANDLE_MODE_CHANGE, _CMD_REAL_TIME_READ_INIT)
    response = GATTResponse()
    self.req.read_by_handle_async(_HANDLE_DATA_READ, response)
    while not response.received():
      time.sleep(0.1)
    rx = response.received()
    self.req.disconnect()
    self.device = device
    self.fields = {
      "temperature": int.from_bytes(rx[0][:2], _BYTE_ORDER) / 10.0,
      "light": int.from_bytes(rx[0][3:7], _BYTE_ORDER),
      "moisture": rx[0][7],
      "conductivity": int.from_bytes(rx[0][8:10], _BYTE_ORDER),
    }

  def __del__(self):
    if self.req: self.req.disconnect()
