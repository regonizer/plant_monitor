#!/usr/bin/env python
from datetime import datetime
from importlib import util
import asyncio
from nio import AsyncClient

class Matrix:
  def __init__(self, username, password, room):
    self.username = username
    self.password = password
    self.room = room
    self.async_client = AsyncClient(
      "https://matrix.org", self.username
    )
    self.cache = {}

  def send(self, msg, details="", throttle=4*60*60):
    if self.username == "myusername": return # disabled
    asyncio.run(self._send(msg, details, throttle))

  async def _send(self, msg, details, throttle):
    try:
      if msg in self.cache and (datetime.now()-self.cache[msg]).seconds < throttle: return
      else: self.cache[msg] = datetime.now()
      await self.async_client.login(self.password)
      content = {
        "body"   : msg+details,
        "msgtype": "m.text"
      }
      await self.async_client.room_send(self.room, "m.room.message", content)
      await self.async_client.close()
    except Exception as e:
      print(e)