#!/usr/bin/env python
import time
from datetime import datetime, timedelta

class Stopwatch:
  def __init__(self, seconds=0, minutes=0, hours=0, days=0):
    self._timestamp = datetime.now() - timedelta(seconds=seconds, minutes=minutes, hours=hours, days=days)

  def is_elapsed(self, seconds):
    print(datetime.now())
    print(self._timestamp + timedelta(seconds=seconds))
    print(datetime.now() >= self._timestamp + timedelta(seconds=seconds))
    return datetime.now() >= self._timestamp + timedelta(seconds=seconds)

  def restart(self, seconds):
    self._timestamp = datetime.now() + timedelta(seconds=seconds)

  def wait_till_elapsed(self, seconds, precision=0.1):
    while not self.is_elapsed(seconds):
      time.sleep(precision)