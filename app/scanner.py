#!/usr/bin/env python
import os
from threading import Thread
import time
from gattlib import DiscoveryService

BTBLE = os.getenv('BTBLE', "hci0")

class Device:
  def __init__(self, mac, name):
    self.mac = mac
    self.name = name

  def __str__(self):
    return f"({self.mac}, {self.name})"

class Scanner:
  def __init__(self):
    self.devices = []
    self.running = True
    self._start()

  def _start(self):
    Thread(target=self._scan).start()
    return self

  def _scan(self):
    service = DiscoveryService(BTBLE)
    while self.running:
      try:
        for device_mac, device_name in service.discover(20).items():
          known = filter(lambda dev: dev.mac==device_mac, self.devices)
          if any(known): # update name
            device = next(known, None)
            if device: device.name = device_name
          else: # new device
            device = Device(device_mac, device_name)
            print(f"found new device: {device}")
            self.devices.append(device)
      except KeyboardInterrupt:
        print('\n')
        self.running = False
      except RuntimeError as e:
        print('Error while trying to list bluetooth devices : {}'.format(e))
      time.sleep(20)