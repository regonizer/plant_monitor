import time
import os
from datetime import datetime
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS

from supervision import Supervision
from matrix import Matrix
from dict_browser import DictBrowser
from scanner import Scanner
from mi.reader import Reader

INFLUXDB = os.getenv('INFLUXDB', "http://localhost:80")
INFLUXDB_TOKEN = os.getenv('INFLUXDB_TOKEN', "unknown")
UPDATE_PERIOD_SEC = int(os.getenv('UPDATE_PERIOD_SEC', 30))
MATRIX_USER = os.getenv('MATRIX_USER', "myusername")
MATRIX_PASSWORD = os.getenv('MATRIX_PASSWORD', "mypassword")
MATRIX_ROOM = os.getenv('MATRIX_ROOM', "mytoken")

matrix = Matrix(MATRIX_USER, MATRIX_PASSWORD, MATRIX_ROOM)
supervision = Supervision(matrix)
scanner = Scanner()

while scanner.running:
  timestamp = datetime.now()
  influxdb = InfluxDBClient(url=INFLUXDB, token=INFLUXDB_TOKEN)
  try:
    influxdb_write_api = influxdb.write_api(write_options=SYNCHRONOUS)
    mi_devices = filter(lambda dev: "Flower care" in dev.name, scanner.devices)
    records = []
    readers = [Reader(device) for device in mi_devices]
    for reader in readers:
      print(f"log device {reader.device}")
      dict_browser = DictBrowser()
      dict_browser.set("measurement", "/plant/flower_care")
      dict_browser.set("time", timestamp)
      dict_browser.set("tags/mac", reader.device.mac)
      dict_browser.set("fields", reader.fields)
      records.append(dict_browser.data)
      if reader.fields["moisture"] < 40:
        matrix.send(f"{reader.device.mac} moisture below 40%: ", reader.fields["moisture"])
    influxdb_write_api.write(bucket="plant", org="plant", record=records)
  except KeyboardInterrupt:
    scanner.running = False
    supervision.running = False
  except Exception as e:
    print(e)
  finally:
    # rerun after timeout
    timediff = timestamp.now() - timestamp
    time.sleep(UPDATE_PERIOD_SEC - timediff.seconds) if UPDATE_PERIOD_SEC > timediff.seconds else None
    influxdb.close()