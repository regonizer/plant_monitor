#!/usr/bin/env python
import json


class DictParser:

  def parse(self, param):
    if isinstance(param, str):
      param = param.split("/")
      param = [x for x in param if x != ""]
    if param is None:
      param = []
    return param

class DictBrowser(DictParser):

  def __init__(self, preset=None):
    if preset is None:
      preset = {}
    self.data = preset

  def __repr__(self):
    return json.dumps(self.data, indent=2)

  def _probe(self, keylist, base_dict):
    if not keylist:
      return True
    target = keylist[0]
    if target in base_dict:
      return self._probe(keylist[1:], base_dict[target])
    return False

  def _get(self, keylist, base_dict):
    if not keylist:
      return base_dict
    target = keylist[0]
    if target in base_dict:
      return self._get(keylist[1:], base_dict[target])

  def _set(self, keylist, value, base_dict):
    target = keylist[0]
    if not keylist[1:]:
      base_dict[target] = value
      return
    if not target in base_dict.keys() or not isinstance(base_dict[target], dict):
      base_dict[target] = {}
    self._set(keylist[1:], value, base_dict[target])

  def _remove(self, keylist, base_dict):
    target = keylist[0]
    if not keylist[1:]:
      del base_dict[target]
      return
    if target in base_dict.keys():
      self._remove(keylist[1:], base_dict[target])

  def probe(self, key):
    return self._probe(self.parse(key), self.data)

  def get(self, key, default=None):
    key = self.parse(key)
    if not self.probe(key):
      if default is None:
        raise ValueError("no such key: /%s" % "/".join(key))
      else:
        return default
    return  self._get(key, self.data)

  def set(self, key, value):
    key = self.parse(key)
    if not key:
      self.data = value
      return
    self._set(key, value, self.data)

  def remove(self, key):
    key = self.parse(key)
    if not key:
      self.data = {}
    self._remove(key, self.data)