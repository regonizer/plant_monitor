#!/usr/bin/env python
"""
example values
{
  "80:EA:CA:60:05:05": {
    "conductivity": {
      "max": 513,
      "min": 495,
      "mean": 503.61306532663315,
      "last": 504,
      "last_update": 1637078364.689246,
      "first_update": 1636991987.107234
    },
    "light": {
      "max": 5492,
      "min": 0,
      "mean": 4088.4854271356785,
      "last": 5377,
      "last_update": 1637078364.689246,
      "first_update": 1636991987.107234
    },
    "moisture": {
      "max": 52,
      "min": 45,
      "mean": 46.65829145728643,
      "last": 46,
      "last_update": 1637078364.689246,
      "first_update": 1636991987.107234
    },
    "temperature": {
      "max": 27.0,
      "min": 25.0,
      "mean": 26.351507537688715,
      "last": 25.3,
      "last_update": 1637078364.689246,
      "first_update": 1636991987.107234
    }
  }
}
"""

import os
import time
from datetime import datetime, timedelta
from threading import Thread
from dict_browser import DictBrowser
from influxdb_client import InfluxDBClient

INFLUXDB = os.getenv('INFLUXDB', "http://localhost:80")
INFLUXDB_TOKEN = os.getenv('INFLUXDB_TOKEN', "unknown")


class Supervision:
  def __init__(self, matrix):
    self._devices = []
    self.running = True
    self.matrix = matrix
    Thread(target=self._check).start()

  def _check(self):
    values = DictBrowser()
    while self.running:
      influxdb = InfluxDBClient(url=INFLUXDB, token=INFLUXDB_TOKEN, timeout=30000)
      try:
        influxdb_query_api = influxdb.query_api()
        results = influxdb_query_api.query('from(bucket: "plant") |> range(start: -24h) |> max()', org="plant")
        for table in results:
          for record in table.records:
            values.set(f"{record.values.get('mac')}/{record.get_field()}/max", record.get_value())
        results = influxdb_query_api.query('from(bucket: "plant") |> range(start: -24h) |> min()', org="plant")
        for table in results:
          for record in table.records:
            values.set(f"{record.values.get('mac')}/{record.get_field()}/min", record.get_value())
        results = influxdb_query_api.query('from(bucket: "plant") |> range(start: -24h) |> mean()', org="plant")
        for table in results:
          for record in table.records:
            values.set(f"{record.values.get('mac')}/{record.get_field()}/mean", record.get_value())
        results = influxdb_query_api.query('from(bucket: "plant") |> range(start: -24h) |> last()', org="plant")
        for table in results:
          for record in table.records:
            values.set(f"{record.values.get('mac')}/{record.get_field()}/last", record.get_value())
        results = influxdb_query_api.query('from(bucket: "plant") |> range(start: -24h) |> last()', org="plant")
        for table in results:
          for record in table.records:
            values.set(f"{record.values.get('mac')}/{record.get_field()}/last_update", record.get_time().timestamp())
        results = influxdb_query_api.query('from(bucket: "plant") |> range(start: -24h) |> first()', org="plant")
        for table in results:
          for record in table.records:
            values.set(f"{record.values.get('mac')}/{record.get_field()}/first_update", record.get_time().timestamp())

        for device in values.get(""):
          if device not in self._devices:
            self._devices.append(device)
            self.matrix.send(f"start supervision for device {device}")
          if values.get(f"{device}/light/max", 2000) < 1000:
            self.matrix.send(f"{device}/light always below 1000lux (past 24h)")
          if values.get(f"{device}/light/min", 100) > 1000:
            self.matrix.send(f"{device}/light always over 1000lux (past 24h)")
          if values.get(f"{device}/moisture/last", 30) < 30: self.matrix.send(f"{device}/moisture below 30")
          if values.get(f"{device}/temperature/last", 20) > 35: self.matrix.send(f"{device}/temperature over 35°C")
          if values.get(f"{device}/temperature/last", 20) < 15: self.matrix.send(f"{device}/temperature below 15°C")
          for value in values.get(f"{device}"):
            delta = datetime.now() - datetime.fromtimestamp(values.get(f"{device}/{value}/last_update"))
            if delta > timedelta(hours=1): self.matrix.send(f"{device}/{value} over 1h offline")
      except Exception as e:
        print(e)
        self.matrix.send(f"supervision error: {e.__class__}")
      finally:
        influxdb.close()
        time.sleep(60 * 10)  # every 10min